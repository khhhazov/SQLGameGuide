package com.example.SQLGameGuide.controller;

import com.example.SQLGameGuide.entities.Guide;
import com.example.SQLGameGuide.models.GuideDto;
import com.example.SQLGameGuide.repo.GuidesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping
public class GuideController {
    @Autowired
    private GuidesRepository guidesRepository;

    @GetMapping("guide")
    public List<Guide> list() {
        return guidesRepository.findAll();
    }

    @GetMapping("guide/{guideName}")
    public Guide loginList(@PathVariable(value = "guideName") String guideName){
        return guidesRepository.findByGuideName(guideName); }

    @PostMapping("guide/create")
    public Guide create(@RequestBody GuideDto guideDto) {
        Guide guidesClass = new Guide(null, guideDto.getGuideName(), guideDto.getGuide());
        return guidesRepository.save(guidesClass);
    }
}
