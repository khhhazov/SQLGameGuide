package com.example.SQLGameGuide.controller;

import com.example.SQLGameGuide.entities.User;
import com.example.SQLGameGuide.models.UserDto;
import com.example.SQLGameGuide.repo.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController()
@RequestMapping
public class UserController {
    @Autowired
    private UsersRepository usersRepository;

//    private int counter = 4;
//    public List<Map<String, String>> users = new ArrayList<Map<String, String>>() {{
//        add(new HashMap<String, String>() {{ put("id", "1"); put("title", "123"); }});
//        add(new HashMap<String, String>() {{ put("id", "2"); put("title", "456"); }});
//        add(new HashMap<String, String>() {{ put("id", "3"); put("title", "789"); }});
//    }};

    @GetMapping("user")
    public List<User> list() {
        return usersRepository.findAll();
    }

    @GetMapping("user/{login}/{password}")
    public User loginList(@PathVariable(value = "login") String login, @PathVariable(value = "password") String password){
        return usersRepository.findByLoginAndPassword(login, password); }

//    @GetMapping("users/{id}")
//    public Map<String, String> getOne(@PathVariable String id) {
//        return getUser(id);
//    }
//    private Map<String, String> getUser(@PathVariable String id) {
//        return user.stream()
//                .filter(user -> user.get("id").equals(id))
//                .findFirst()
//                .orElseThrow(NotFoundException::new);
//    }

    @PostMapping("user/create")
    public User create(@RequestBody UserDto users) {
        User usersClass = new User(null, users.getLogin(), users.getPassword());
        return usersRepository.save(usersClass);
    }

//    @DeleteMapping("users/{login}")
//    public void delete(@PathVariable String id) {
//        Map<String, String> user = getUser(id);
//
//        users.remove(user);
//    }
}


