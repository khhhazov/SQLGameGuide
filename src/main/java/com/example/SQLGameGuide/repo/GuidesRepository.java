package com.example.SQLGameGuide.repo;

import com.example.SQLGameGuide.entities.Guide;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GuidesRepository extends JpaRepository<Guide, Long> {
    Guide findByGuideName(String guideName);
}
