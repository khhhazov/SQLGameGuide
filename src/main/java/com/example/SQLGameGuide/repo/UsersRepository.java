package com.example.SQLGameGuide.repo;

import com.example.SQLGameGuide.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<User, Long> {
//    Optional<Users> findByLoginAndPassword(String login, String password);
    User findByLoginAndPassword(String login, String password);
}
