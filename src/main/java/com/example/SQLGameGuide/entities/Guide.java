package com.example.SQLGameGuide.entities;

import javax.persistence.*;

@Entity
@Table(name = "guides")
public class Guide {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String guideName;
    private String guide;

    public Guide(Long id, String guideName, String guide) {
        this.id = id;
        this.guideName = guideName;
        this.guide = guide;
    }

    public Guide(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGuideName() {
        return guideName;
    }

    public void setGuideName(String guideName) {
        this.guideName = guideName;
    }

    public String getGuide() {
        return guide;
    }

    public void setGuide(String guide) {
        this.guide = guide;
    }
}
