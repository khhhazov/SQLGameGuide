package com.example.SQLGameGuide;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SqlGameGuideApplication {

	public static void main(String[] args) {
		SpringApplication.run(SqlGameGuideApplication.class, args);
	}

}
